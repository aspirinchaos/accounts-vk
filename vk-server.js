import { ServiceConfiguration } from 'meteor/service-configuration';
import { OAuth } from 'meteor/oauth';
import { HTTP } from 'meteor/http';

const getIdentity = (accessToken, fields) => {
  const config = ServiceConfiguration.configurations.findOne({ service: 'vk' });
  if (!config) {
    throw new ServiceConfiguration.ConfigError();
  }

  try {
    const result = HTTP.get('https://api.vk.com/method/users.get', {
      params: {
        v: '5.80',
        access_token: accessToken,
        fields: fields.join(','),
      },
    });

    if (result.error) {
      throw result.error;
    }
    return result.data.response[0];
  } catch (err) {
    throw Object.assign(
      new Error(`Failed to fetch identity from VK. ${err.message}`),
      { response: err.response },
    );
  }
};

const VK = {
  retrieveCredential(credentialToken) {
    return OAuth.retrieveCredential(credentialToken);
  },
  handleAuthFromAccessToken(accessToken, expiresAt, email) {
    // include basic fields from vk
    // https://vk.com/dev/users.get
    const whitelisted = ['id', 'nickname', 'first_name', 'last_name', 'sex',
      'bdate', 'timezone', 'photo', 'photo_big', 'city', 'country'];

    const identity = getIdentity(accessToken, whitelisted);

    const serviceData = {
      accessToken,
      expiresAt,
    };

    const fields = whitelisted.reduce((o, k) => Object.assign(o, { [k]: identity[k] }), {});
    Object.assign(serviceData, fields);
    if (email) {
      serviceData.email = email;
    }

    return {
      serviceData,
      options: {
        profile: {
          name: `${identity.first_name} ${identity.last_name}`,
        },
      },
    };
  },
};

// returns an object containing:
// - accessToken
// - expiresIn: lifetime of token in seconds
const getTokenResponse = (query) => {
  const config = ServiceConfiguration.configurations.findOne({ service: 'vk' });
  if (!config) {
    throw new ServiceConfiguration.ConfigError('Service not configured');
  }

  let responseContent;

  try {
    // Request an access token
    responseContent = HTTP.post('https://api.vk.com/oauth/access_token', {
      params: {
        client_id: config.appId,
        redirect_uri: OAuth._redirectUri('vk', config),
        client_secret: OAuth.openSecret(config.secret),
        code: query.code,
      },
    }).content;
  } catch (err) {
    throw Object.assign(
      new Error(`Failed to complete OAuth handshake with VK. ${err.message}`),
      { response: err.response },
    );
  }

  const parsedResponse = JSON.parse(responseContent);

  const vkAccessToken = parsedResponse.access_token;
  const vkExpires = parsedResponse.expires_in;

  if (!vkAccessToken) {
    throw new Error(`${'Failed to complete OAuth handshake with VK ' +
    '-- can\'t find access token in HTTP response. '}${responseContent}`);
  }
  return {
    accessToken: vkAccessToken,
    expiresIn: vkExpires,
    email: parsedResponse.email || false,
  };
};

OAuth.registerService('vk', 2, null, (query) => {
  const response = getTokenResponse(query);
  const { accessToken, expiresIn, email } = response;

  return VK.handleAuthFromAccessToken(accessToken, (+new Date()) + (1000 * expiresIn), email);
});

export { VK };
