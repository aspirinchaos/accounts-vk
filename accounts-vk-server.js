import { Accounts } from 'meteor/accounts-base';

Accounts.oauth.registerService('vk');

Accounts.addAutopublishFields({
  forLoggedInUser: ['services.vk'],
  forOtherUsers: [
    'services.vk.id',
    'services.vk.nickname',
    'services.vk.gender',
  ],
});
