# OAuth авторизация дл VK

Пакет основан на [facebook-oauth](https://github.com/meteor/meteor/tree/master/packages/facebook-oauth) и [accounts-facebook](https://github.com/meteor/meteor/tree/master/packages/accounts-facebook)

Код переписан под es6 и убраны лишние зависимости.

Пакет не будут использоваться по отдельности, поэтому были объединены в один.

## Использование
```javascript
Meteor.loginWithVK({
  requestPermissions: ['email']
}, (err) => {
  if (err) {
    // handle error
  } else {
    // successful login!
  }
});
``` 
