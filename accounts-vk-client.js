import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { VK } from './vk-client';

Accounts.oauth.registerService('vk');

const loginWithVK = (options, callback) => {
  // support a callback without options
  if (!callback && typeof options === 'function') {
    callback = options;
    options = null;
  }

  const credentialRequestCompleteCB = Accounts.oauth.credentialRequestCompleteHandler(callback);
  VK.requestCredential(options, credentialRequestCompleteCB);
};

Accounts.registerClientLoginFunction('vk', loginWithVK);

Meteor.loginWithVK = (...args) => Accounts.applyLoginFunction('vk', args);
