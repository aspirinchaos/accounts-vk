// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from 'meteor/tinytest';

// Import and rename a variable exported by accounts-vk.js.
import { name as packageName } from 'meteor/accounts-vk';

// Write your tests here!
// Here is an example.
Tinytest.add('accounts-vk - example', function (test) {
  test.equal(packageName, 'accounts-vk');
});
