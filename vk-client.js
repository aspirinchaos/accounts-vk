import { ServiceConfiguration } from 'meteor/service-configuration';
import { Random } from 'meteor/random';
import { OAuth } from 'meteor/oauth';

const VK = {
  requestCredential(options, credentialRequestCompleteCallback) {
    if (!credentialRequestCompleteCallback && typeof options === 'function') {
      credentialRequestCompleteCallback = options;
      options = {};
    }

    const config = ServiceConfiguration.configurations.findOne({ service: 'vk' });
    if (!config) {
      if (credentialRequestCompleteCallback) {
        credentialRequestCompleteCallback(new ServiceConfiguration.ConfigError('Service not configured'));
        return;
      }
    }

    const credentialToken = Random.id();
    const mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent);
    const display = mobile ? 'touch' : 'popup';

    let scope = 'email';
    if (options && options.requestPermissions) {
      scope = options.requestPermissions.join(',');
    }

    const loginStyle = OAuth._loginStyle('vk', config, options);

    const loginUrl =
      `https://oauth.vk.com/authorize?client_id=${config.appId
      }&redirect_uri=${OAuth._redirectUri('vk', config)
      }&display=${display}&scope=${scope
      }&state=${OAuth._stateParam(loginStyle, credentialToken, options && options.redirectUrl)}`;

    OAuth.launchLogin({
      loginService: 'vk',
      loginStyle,
      loginUrl,
      credentialRequestCompleteCallback,
      credentialToken,
    });
  },
};

export { VK };
