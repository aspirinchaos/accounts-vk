Package.describe({
  name: 'accounts-vk',
  version: '0.1.0',
  summary: 'oAuth for VK',
  git: '',
  documentation: 'README.md',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use(['ecmascript', 'oauth2', 'random', 'oauth', 'service-configuration']);
  api.use('http', ['server']);
  api.mainModule('vk-server.js', 'server');
  api.mainModule('vk-client.js', 'client');
  api.addFiles('accounts-vk-server.js', 'server');
  api.addFiles('accounts-vk-client.js', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('accounts-vk');
  api.mainModule('accounts-vk-tests.js');
});
